package comm

import (

)

type RegistrationDescriptor struct {
    Name        string
    RpcHost     string
    RpcPort     string
    WebAppCall  string
    ContextPath string
}