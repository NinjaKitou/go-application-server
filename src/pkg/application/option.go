package application

import ()

type Option struct {
    shortname        string
    longname         string
    shortDescription string
    description      string
    hasValue         bool
    optional         bool
    validator        func(option string) (bool,error)
    reader           func(option string)
}

func MakeOption(shortname        string,
                longname         string,
                shortDescription string,
                description      string,
                hasValue         bool,
                optional         bool,
                validator        func(option string) (bool,error),
                reader           func(option string)) Option {
    return Option{shortname, longname, shortDescription, description, hasValue, optional, validator, reader}
}
