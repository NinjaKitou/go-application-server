package application

import (
    "os"
	"strconv"
	"net"
	"errors"
	
	"pkg/log"
)

func validateInt(strInt string, bitSize int) (bool, error) {
	_,err := strconv.ParseInt(strInt, 0, bitSize)
	if (nil != err) {
	    return false, err
	}
	return true, nil
}

func validateUInt(strInt string, bitSize int) (bool, error) {
	_,err := strconv.ParseUint(strInt, 0, bitSize)
	if (nil != err) {
	    return false, err
	}
	return true, nil
}

func ValidateInt64(strInt string) (bool, error) {
    log.Trace.Println("Validating Int64")
	return validateInt(strInt, 64)
}

func ValidateInt32(strInt string) (bool, error) {
    log.Trace.Println("Validating Int32")
	return validateInt(strInt, 32)
}

func ValidateInt16(strInt string) (bool, error) {
    log.Trace.Println("Validating Int16")
	return validateInt(strInt, 16)
}

func ValidateUInt64(strInt string) (bool, error) {
    log.Trace.Println("Validating UInt64")
	return validateUInt(strInt, 64)
}

func ValidateUInt32(strInt string) (bool, error) {
    log.Trace.Println("Validating UInt32")
	return validateUInt(strInt, 32)
}

func ValidateUInt16(strInt string) (bool, error) {
    log.Trace.Println("Validating UInt16")
	return validateUInt(strInt, 16)
}

func ValidateIP(strIP string) (bool, error) {
    log.Trace.Println("Validating IP")
    var ipAddress net.IP = net.ParseIP(strIP)
    if nil == ipAddress {
        log.Error.Println(strIP + " is not a valid IP")
        err := errors.New("Not a valid IP")
        return false, err
    }
    return true, nil
}

func ValidateDir(path string) (bool, error) {
    log.Trace.Println("Validating Directory")
    _, err := os.Stat(path)
    if err == nil { return true, nil }
    log.Error.Println(path + " is not a valid Directory")
    if os.IsNotExist(err) { return false, nil }
    return false, err
}

func DoNotValidate(strInt string) (bool, error) {
    log.Trace.Println("Validating Nothing")
	return true,nil
}