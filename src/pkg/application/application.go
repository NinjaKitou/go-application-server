package application

import (
    "fmt"
    
    "pkg/log"
)

type Application struct {
    args          []string
    options       []Option
    parsedOptions map[string]string
    handlers      []func() int
}

func MakeApplication(args []string) Application {
    log.Trace.Println("Instanciating Application in stack")
    return Application{args: args, parsedOptions: make(map[string]string)}
}

func NewApplication() *Application {
    log.Trace.Println("Instanciating Application in heap")
    return new(Application)
}

func (application *Application) Init(args []string) {
    log.Trace.Println("Initializing Application object")
    application.args          = args
    application.parsedOptions = make(map[string]string)
}

func (application *Application) AddOption(option Option) {
    log.Info.Println("Adding option: " + option.shortname + " to the application context")
    application.options = append(application.options, option)
}

func (application *Application) AddProcess(process func()(int)) {
    log.Info.Println("Adding process to the application context")
    application.handlers = append(application.handlers, process)
}

func (application *Application) Parse() bool {
    log.Trace.Println("Parsing options")
    /*
     * Loop on each options and command line argument.
     * Check if the option as a value:
     * - if the option has a value:
     *     - Check if the value is provided
     *     - Check validity of the value
     *     - Register the option with the value
     * - if not just register the option
     */
    for _, option := range application.options {
        for argIndex, argument := range application.args {
            if option.shortname == argument ||
                option.longname == argument {
                if option.hasValue {
                    if len(application.args) <= argIndex+1 {
                        log.Error.Println("Failed to read: " + option.shortname + " Reason: no value provided")
                        application.usage()
                        return false
                    }

                    var optionValue string = application.args[argIndex+1]
                    isValid, err := option.validator(optionValue)
                    if !isValid {
                        log.Error.Println("Failed to read: " + option.shortname + " Reason: " + err.Error())
                        application.usage()
                        return false
                    }
                    application.parsedOptions[option.shortname] = optionValue
                } else {
                    application.parsedOptions[option.shortname] = ""
                }
            }
        }
    }
    return application.checkOptions()
}

func (application *Application) Process() int {
    log.Info.Println("Starting processing loop")
    for _,process := range application.handlers {
        if ret := process(); ret != 0 {
            return ret
        }
    }
    return 0
}

func (application *Application) checkOptions() bool {
    for _, option := range application.options {
        if !option.optional {
            _, hasValue := application.parsedOptions[option.shortname]
            if !hasValue {
                log.Error.Println("Mandatory option not found: " + option.shortname)
                application.usage()
                return false
            }
        }
        option.reader(application.parsedOptions[option.shortname])
    }
    return true
}

func (application *Application) usage() {
    fmt.Print("Usage: ")
    fmt.Print(application.args[0] + " ")
    for _, option := range application.options {
        if !option.optional {
            if option.hasValue {
                fmt.Printf("%s <%s> ", option.shortname, option.shortDescription)
            } else {
                fmt.Printf("%s ", option.shortname)
            }
        }
    }

    for _, option := range application.options {
        if option.optional {
            if option.hasValue {
            	fmt.Printf("[%s <%s>] ", option.shortname, option.shortDescription)
            } else {
                fmt.Printf("[%s] ", option.shortname)
            }
        }
    }

    fmt.Print("\n\n")
    for _, option := range application.options {
        fmt.Printf("%12s,%s:\n", option.shortname, option.longname)
        fmt.Printf("%20s%s\n", "", option.description)
    }
}
