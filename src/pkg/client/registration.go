package client

import (
    "bytes"
    "encoding/json"
    "net/http"
    
	"pkg/comm"
	"pkg/log"
)

type RegistrationClient struct {
    server    string
    port      string
    descriptor comm.RegistrationDescriptor
}

func MakeRegistrationClient(
    server string,
    port   string,
    descriptor comm.RegistrationDescriptor) RegistrationClient {
        log.Trace.Println("Instanciating RegistrationClient in stack")
        return RegistrationClient{server: server, port: port, descriptor: descriptor}
}

func NewRegistrationClient() *RegistrationClient {
    log.Trace.Println("Instanciating RegistrationClient in heap")
    return new(RegistrationClient)
}

func (registrationClient *RegistrationClient) Init(
    server string,
    port   string,
    descriptor comm.RegistrationDescriptor) {
    log.Trace.Println("Initializing RegistrationClient")
    registrationClient.server     = server
    registrationClient.port       = port
    registrationClient.descriptor = descriptor
}


func (registrationClient *RegistrationClient) Process() int {
    log.Info.Println("Registration client processing")
        var out []byte
    var buffer *bytes.Buffer = bytes.NewBuffer(out)
	encoder := json.NewEncoder(buffer)
	
	encodeErr := encoder.Encode(registrationClient.descriptor)

    if nil != encodeErr {
        log.Error.Println("Problem occured while marshalling json: " + encodeErr.Error())
        return 1
    }
    
    req, httpErr := http.NewRequest("POST", "http://localhost:8181/register/", buffer)
    req.Header.Set("Content-Type", "application/json")
    if nil != httpErr {
        log.Error.Println("Problem occured while creating http request: " + httpErr.Error())
        return 2
    }
    
    client := &http.Client{}
    resp, clientErr := client.Do(req)
    
    if nil != clientErr {
        log.Error.Println("Problem occured while sending http request: " + clientErr.Error())
        return 3
    }
    
    if resp.Status != "200 OK" {
        log.Error.Println("Problem server replied with status: " + resp.Status)
        return 4
    }
    return 0
}