package server

import (
    "fmt"
    "net/http"
    "net/rpc"
    "encoding/json"
    
    "pkg/application"
    "pkg/webapp"
    "pkg/log"
    "pkg/comm"
)

type WebApplicationServer struct {
    app    *application.Application
    server *HttpServer
}

func MakeWebApplicationServer(args []string) WebApplicationServer {
    
    log.Trace.Println("Instanciating WebApplicationServer in stack")
    app    := application.NewApplication()
    app.Init(args)
    
    server := NewHttpServer()
    server.Init()
    
    var webAppServer WebApplicationServer
    webAppServer.app    = app
    webAppServer.server = server
    
    var portOption application.Option = application.MakeOption("-p",
                                                               "--port",
                                                               "listen port",
                                                               "Application server listen port",
                                                               true,
                                                               false,
                                                               application.ValidateUInt32,
                                                               webAppServer.readPort)
    
    var hostOption application.Option = application.MakeOption("-a",
                                                               "--address",
                                                               "bind address",
                                                               "Application server address",
                                                               true,
                                                               false,
                                                               application.ValidateIP,
                                                               webAppServer.readHost)
    
    var dirOption application.Option = application.MakeOption("-d",
                                                               "--directory",
                                                               "static files directory",
                                                               "Directory containing static files to serve",
                                                               true,
                                                               false,
                                                               application.ValidateDir,
                                                               webAppServer.readStaticFilesDir)
    
    
    app.AddOption(portOption)
	app.AddOption(hostOption)
	app.AddOption(dirOption)
	app.AddProcess(webAppServer.registrationProcess)
	app.AddProcess(server.Process)
	
    return webAppServer
}

func (webAppServer *WebApplicationServer) Start() int {
    log.Info.Println("Starting web application server")
    if webAppServer.app.Parse() {
	    return webAppServer.app.Process()
	} else {
	    return -1
	}
}

func (webAppServer *WebApplicationServer) readPort(option string) {
    log.Trace.Println("Port: " + option)
    webAppServer.server.port = option
}

func (webAppServer *WebApplicationServer) readHost(option string) {
    log.Trace.Println("Host: " + option)
    webAppServer.server.host = option
}

func (webAppServer *WebApplicationServer) readStaticFilesDir(option string) {
    log.Trace.Println("Static files dir: " + option)
    webAppServer.server.staticFilesPath = option
}

func (webAppServer *WebApplicationServer) registrationProcess() int {
    log.Info.Println("Registration process started")
    
    var webApp webapp.WebApp = webapp.MakeWebApp("/register/", webAppServer.registrationHandler)
    webAppServer.RegisterApp(webApp)
    
    return 0
}


func (webAppServer *WebApplicationServer) registrationHandler(w http.ResponseWriter, r *http.Request) {
    log.Access.Println(r.Method + " " + r.URL.Path + " " + r.UserAgent())
    if "POST" == r.Method {
        var regDescriptor comm.RegistrationDescriptor
        
        decoder := json.NewDecoder(r.Body)
        
        decoderErr := decoder.Decode(&regDescriptor)
        
        if (nil != decoderErr) {
            log.Error.Println("Error unmarshaling: " + decoderErr.Error())
        }
        log.Info.Println("Registration: " + regDescriptor.RpcHost + ":" + regDescriptor.RpcPort + " " + regDescriptor.WebAppCall + " " + regDescriptor.ContextPath)
        
        /*
         * Creates the handler wrapping RPC call into http server context.
         */
        rpcHandler := func(res http.ResponseWriter, req *http.Request) {
            log.Access.Println(req.Method + " " + req.URL.Path + " " + req.UserAgent())
            client, err := rpc.DialHTTP("tcp", regDescriptor.RpcHost + ":" + regDescriptor.RpcPort)
            
            if err != nil {
				log.Error.Println("Dialing " + regDescriptor.RpcHost + ":" + regDescriptor.RpcPort + " error: " + err.Error())
			}
            rcpReq := webapp.Request{req.Method, req.URL.Path}
            var rcpRes webapp.Response
            errCall := client.Call(regDescriptor.WebAppCall, rcpReq, &rcpRes)
            
            if errCall != nil {
				log.Error.Println("Calling rpc problem: " + errCall.Error())
			}
            res.WriteHeader(rcpRes.Status)
            fmt.Fprintf(res, rcpRes.Body)

        }
        
        webAppServer.RegisterApp(webapp.MakeWebApp(regDescriptor.ContextPath, rpcHandler))
    }
}

func (webAppServer *WebApplicationServer) testHandler(w http.ResponseWriter, r *http.Request) {
    log.Access.Println(r.Method + " " + r.URL.Path + " " + r.UserAgent())
    fmt.Fprintf(w, "<h1>REGISTERED</h1>")
}


func (webAppServer *WebApplicationServer) RegisterApp(webApp webapp.WebApp) {
    webAppServer.server.addHandler(webApp.GetContextPath(), webApp.GetHandler())
}

