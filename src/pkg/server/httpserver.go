package server

import (
    "os"
	"fmt"
	"net/http"
	
	"pkg/log"
)

type HttpServer struct {
	host            string
	port            string
	staticFilesPath string
	chttp           *http.ServeMux
}

func MakeHttpServer() HttpServer {
    log.Trace.Println("Instanciating HttpServer in stack")
	return HttpServer{chttp: http.NewServeMux()}
}

func NewHttpServer() *HttpServer {
    log.Trace.Println("Instanciating HttpServer in heap")
	return new(HttpServer)
}

func (httpServer *HttpServer) Init() {
    log.Trace.Println("Initializing HttpServer")
	httpServer.chttp = http.NewServeMux()
}

func (httpServer *HttpServer) addHandler(contextPath string, handler func(w http.ResponseWriter, r *http.Request)) {
    log.Info.Println("Adding web application handler with context path: " + contextPath)
    http.HandleFunc(contextPath, handler)
}

func (httpServer *HttpServer) homeHandler(w http.ResponseWriter, r *http.Request) {
    log.Access.Println(r.Method + " " + r.URL.Path + " " + r.UserAgent())
	log.Trace.Println("Call home handler")
    var path string;
	if ("/" == r.URL.Path) {
	    path = "/index.html"
	} else {
	    path = r.URL.Path
	}
	
	var filename string = httpServer.staticFilesPath + path
	
	log.Trace.Println("Verifying file " + filename + " exists")
	if _, err := os.Stat(filename); err == nil {
    	httpServer.chttp.ServeHTTP(w, r)
	} else {
		fmt.Fprintf(w, "<h1>Welcome to GO app Server:</h1>"+
			"<br /><h3>Please add index.html file in "+httpServer.staticFilesPath+" to override this default page</h3>")
	}
}

func (httpServer *HttpServer) Process() int {

	log.Info.Println("Starting HTTP server processing, listen: " + httpServer.host + ":" + httpServer.port)
	httpServer.chttp.Handle("/", http.FileServer(http.Dir(httpServer.staticFilesPath)))

	http.HandleFunc("/", httpServer.homeHandler)

	err := http.ListenAndServe(httpServer.host+":"+httpServer.port, nil)
	if nil != err {
	    log.Error.Println("Error occured while trying to start HTTP server: " + err.Error())
		return -1
	}
	return 0
}
