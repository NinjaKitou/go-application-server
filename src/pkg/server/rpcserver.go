package server

import (
    "net"
	"net/rpc"
	"net/http"
	"pkg/log"
)

type RpcServer struct {
    host string
    port string
}

func MakeRpcServer(
    host string,
    port string) RpcServer {
    return RpcServer{host: host, port: port}
}


func NewRpcServer() *RpcServer {
    return new(RpcServer)
}

func (rpcServer *RpcServer) Init(host string, port string) {
    rpcServer.host = host
    rpcServer.port = port
}

func (rpcServer *RpcServer) RegisterObject(object interface {}) {
    log.Info.Println("Register new object")
    rpc.Register(object)
}

func (rpcServer *RpcServer) Process() int {
    log.Info.Println("Start rpc server: " + rpcServer.host + ":" + rpcServer.port)
    rpc.HandleHTTP()
    listener, err := net.Listen("tcp", rpcServer.host + ":" + rpcServer.port)
    if nil != err {
        log.Error.Println("could not listen: " + err.Error())
    	return 1
	}
    http.Serve(listener, nil)
    return 0
}