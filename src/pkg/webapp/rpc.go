package webapp

import (
	"pkg/log"
)

type Response struct {
    Body   string
    Status int
}

type Request struct {
    Method string
    Path   string
}

type RpcApp struct {
    Process func(req Request, res *Response) error
}

func MakeRpcApp(process func(req Request, res *Response) error) RpcApp {
    return RpcApp{process}
}

func NewRpcApp(process func(req Request, res *Response) error) *RpcApp {
    return &RpcApp{process}
}

func (app *RpcApp) ProcessRequest(req Request, res *Response) error {
    log.Access.Println(req.Method + " " + req.Path)
    app.Process(req, res)
    return nil
}