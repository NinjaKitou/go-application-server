package webapp

import (
	"net/http"
)

type WebApp struct {
    contextPath string
    handler     func(w http.ResponseWriter, r *http.Request)
}

func MakeWebApp(contextPath string, handler func(w http.ResponseWriter, r *http.Request)) WebApp {
    return WebApp{contextPath, handler}
}

func (webApp *WebApp) GetContextPath() string {
    return webApp.contextPath
}

func (webApp *WebApp) GetHandler() func(w http.ResponseWriter, r *http.Request) {
    return webApp.handler
}