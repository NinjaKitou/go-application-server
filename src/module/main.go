package main 

import (
    "os"
    "time"
    "io/ioutil"
    
    "pkg/log"
    "pkg/comm"
    "pkg/application"
    "pkg/client"
    "pkg/server"
    "pkg/webapp"
)

var rpcHost      string
var rpcPort      string
var rpcInterface string
var httpHost     string
var httpPort     string

func main() {
    log.InitLog(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr, os.Stdout)
    log.Info.Println("Starting GO module")
    
    var rpcPortOption application.Option = application.MakeOption("-rpcp",
                                                               "--rpc-port",
                                                               "RPC listen port",
                                                               "Module RPC server listen port",
                                                               true,
                                                               false,
                                                               application.ValidateUInt32,
                                                               readRPCPort)
    
    var rpcHostOption application.Option = application.MakeOption("-rpca",
                                                               "--rpc-address",
                                                               "RPC bind address",
                                                               "Module RPC server address",
                                                               true,
                                                               false,
                                                               application.ValidateIP,
                                                               readRPCHost)
    
    var rpcInterfaceOption application.Option = application.MakeOption("-rpci",
                                                               "--rpc-inteface-addr",
                                                               "RPC interface address",
                                                               "Network Interface Address for RPC module",
                                                               true,
                                                               false,
                                                               application.ValidateIP,
                                                               readInterfaceAddr)
    
    var portOption application.Option = application.MakeOption("-p",
                                                               "--port",
                                                               "listen port",
                                                               "Application server listen port",
                                                               true,
                                                               false,
                                                               application.ValidateUInt32,
                                                               readPort)
    
    var hostOption application.Option = application.MakeOption("-a",
                                                               "--address",
                                                               "bind address",
                                                               "Application server address",
                                                               true,
                                                               false,
                                                               application.ValidateIP,
                                                               readHost)
    
    app := application.NewApplication()
    app.Init(os.Args)
    
    app.AddOption(rpcPortOption)
	app.AddOption(rpcHostOption)
	app.AddOption(rpcInterfaceOption)
    app.AddOption(portOption)
	app.AddOption(hostOption)
	app.AddProcess(process)
	
	if app.Parse() {
	    os.Exit(app.Process())
	} else {
	    os.Exit(-1)
	}

    log.Info.Println("End GO module")
}

func process() int {
    regDescriptor := comm.RegistrationDescriptor{"First Module", rpcInterface, rpcPort, "RpcApp.ProcessRequest", "/hello/"}
    registrationClient := client.MakeRegistrationClient(httpHost, httpPort, regDescriptor)

	rpcServer := server.MakeRpcServer(rpcHost, rpcPort)
	
	app := new(webapp.RpcApp)
	app.Process = handler
	rpcServer.RegisterObject(app)
	
	routineQuit := make(chan int)
    go routine(routineQuit, &rpcServer)
	
    for {
        if clientRet := registrationClient.Process(); 0 == clientRet {
            log.Info.Println("Registration successful")
            break;
        }
        log.Info.Println("Registration failed. Retrying in 5 seconds")
        time.Sleep(time.Second * 5)
    }
    
    return <-routineQuit
}

func handler(req webapp.Request, res *webapp.Response) error {
    res.Status = 200
    res.Body = "<h1>Hello, Module 1</h1>"
    return nil
}

func routine(quit chan int, rpcServer *server.RpcServer) {
    quit <- rpcServer.Process()
}

func readRPCPort(option string) {
    rpcPort = option
}

func readRPCHost(option string) {
    rpcHost = option
}

func readPort(option string) {
    httpPort = option
}

func readHost(option string) {
    httpHost = option
}

func readInterfaceAddr(option string) {
    rpcInterface = option
}



