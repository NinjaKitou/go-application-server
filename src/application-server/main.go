package main

import (
    "os"
    "io/ioutil"
    "strconv"
	
	"pkg/log"
	"pkg/server"
)

func main() {
    log.InitLog(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr, os.Stdout)
    log.Info.Println("Starting GO application server")
    
    var appServer server.WebApplicationServer = server.MakeWebApplicationServer(os.Args)
    
    var ret int = appServer.Start()
    
    log.Info.Println("Exiting GO application server")
    log.Info.Println("Exit value: " + strconv.Itoa(ret))
    os.Exit(ret)
}
