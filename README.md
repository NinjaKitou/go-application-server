# Modular GO Web Application Server #

### Summary ###

This project is an example of GO usage.
It produces 2 binaries:

* The application server.
* A module example.

It also produce a set of libraries used by the binaries.

### Set up ###

#### Ensure that GO is intalled and correctly set up ####
* GOROOT environment variable should point to GO installation.
* GO binaries directory should be in the PATH.

#### Build ####
Two options:

* Command
```
#!bash

go install -v -gcflags "-N -l" ./...
```

* Gradle
```
#!bash

gradlew install
```
#### Run the binaries (in bin folder) ####
